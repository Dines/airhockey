#define WHITE al_map_rgb(255, 255, 255)
#define BLACK al_map_rgb(0, 0, 0)
#define RED al_map_rgb(255, 0, 0)
#define GREEN al_map_rgb(0, 255, 0)
#define BLUE al_map_rgb(0, 0, 255)
#define ORANGE al_map_rgb(255, 165, 0)

#include <math.h>
#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

//Original game has 700 width and 400 height

typedef struct{
	float x, y;
} Vector2D;

typedef struct{
	Vector2D c1, c2; 
} Box;

typedef struct{
	Vector2D pos, vel;
	float r;

} CircularBody;

const int 	stick_r = 25;

const float	dt = 1.0/300.0,
			stick_m = 5,
			puck_m = 15,
			arena_ration = 7.0/4.0,
			stick_max_v = 7500;

float stick_ai_v;//1500

const Vector2D screen = {.x = 500, .y = 800};
const Box arena = {.c1.x = 100, .c1.y = 100, .c2.x = 400, .c2.y = 700}; 

Vector2D add(Vector2D a, Vector2D b){
	a.x+=b.x;
	a.y+=b.y;
	return a;
}

ALLEGRO_FONT *font = NULL;

Vector2D sub(Vector2D a, Vector2D b){
	a.x-=b.x;
	a.y-=b.y;
	return a;
}

float dis(Vector2D a, Vector2D b){
	return sqrt(pow(a.x-b.x,2)+pow(a.y-b.y,2));
}

float len(Vector2D a){
	Vector2D b = {.x = 0, .y = 0};
	return dis(a, b);
}

Vector2D nor(Vector2D a){
	float l = len(a);
	if(l==0) return a;
	a.x/=l;
	a.y/=l;
	return a;
}

Vector2D mul(Vector2D a, float m){
	a.x*=m;
	a.y*=m;
	return a;
}

Vector2D division(Vector2D a, float d){
	a.x/=d;
	a.y/=d;
	return a;
}

void printVec(Vector2D a){
	printf("x = %f, y = %f\n", a.x, a.y);
}

void collision_with_stick(Vector2D pos1, Vector2D pos2, float r1, float r2, Vector2D *vel1, Vector2D *vel2, float m1, float m2){
	
	if(dis(pos1, pos2)<r1+r2){

		float newCollisionVel = len(*vel1) + len(*vel2)*0.9;

		if(newCollisionVel>4500){
			newCollisionVel = 5000;
		}

		Vector2D dir = nor(sub(pos2, pos1));
		*vel2 =  mul(dir, newCollisionVel);

	}
}

void collision_with_wall(Vector2D pos, Vector2D *vel, Box Arena, int r){

	if(pos.x<=arena.c1.x+r) (*vel).x=abs((*vel).x);
	if(pos.x>=arena.c2.x-r) (*vel).x=-abs((*vel).x);
	if(pos.y<=arena.c1.y+r) (*vel).y=abs((*vel).y);
	if(pos.y>=arena.c2.y-r) (*vel).y=-abs((*vel).y);
}

Vector2D find_position_optimized(Vector2D pos, Box arena, int r){

	if(pos.x<arena.c1.x+r) pos.x = arena.c1.x+r;
	if(pos.x>arena.c2.x-r) pos.x = arena.c2.x-r;
	if(pos.y<arena.c1.y+r) pos.y = arena.c1.y+r;
	if(pos.y>arena.c2.y-r) pos.y = arena.c2.y-r;

	return pos;
}

int collision_with_goal(Vector2D pos, int left, int right, int r){
	if(pos.y<arena.c1.y+r && left<pos.x && pos.x<right){
		return 1;
	}
	else if(pos.y>arena.c2.y-r && left<pos.x && pos.x<right){
		return 2;
	}
	return 0;
}

void draw_menu(){
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250, screen.x/2+125, 250+60, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-50, 250+7, 0, "Play");
    
    /*al_draw_filled_rounded_rectangle(screen.x/2-125, 250+100, screen.x/2+125, 250+160, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-90, 250+100+7, 0, "Settings");*/
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+200, screen.x/2+125, 250+260, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-45, 250+200+7, 0, "Exit");
}

void draw_diff(){
	
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250, screen.x/2+125, 250+60, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-50, 250+7, 0, "Easy");
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+100, screen.x/2+125, 250+160, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-90, 250+100+7, 0, "  Normal ");
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+200, screen.x/2+125, 250+260, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-50, 250+200+7, 0, "Hard");	
}

void draw_bot_win(){
	
    al_draw_textf(font, RED, screen.x/2-130, 250+7, 0, "Computer won");
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+100, screen.x/2+125, 250+160, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-60, 250+100+7, 0, "Again");
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+200, screen.x/2+125, 250+260, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-45, 250+200+7, 0, "Exit");	
}

void draw_player_win(){
	
    al_draw_textf(font, BLUE, screen.x/2-110, 250+7, 0, "Player won");
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+100, screen.x/2+125, 250+160, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-60, 250+100+7, 0, "Again");
    
    al_draw_filled_rounded_rectangle(screen.x/2-125, 250+200, screen.x/2+125, 250+260, 20, 20, BLACK);
    al_draw_textf(font, WHITE, screen.x/2-45, 250+200+7, 0, "Exit");	
}


int main() {

	srand(time(NULL));
	al_init();
	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();

	font = al_load_font("./src/media/font/NemoyBold.otf", 32, 0);

	ALLEGRO_DISPLAY *display = al_create_display(screen.x, screen.y);
	ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue();
	ALLEGRO_TIMER *timer = al_create_timer(dt);
	al_start_timer(timer);
	al_install_keyboard();
	al_install_mouse();

	al_register_event_source(queue, al_get_display_event_source(display));
	al_register_event_source(queue, al_get_keyboard_event_source());
	al_register_event_source(queue, al_get_mouse_event_source());
	al_register_event_source(queue, al_get_timer_event_source(timer));

	ALLEGRO_EVENT ev;

	const Vector2D ai_default_stick_pos = {.x = 250, .y = 200};

	CircularBody puck = {.pos.x = 250, .pos.y = 450, .vel.x = 0, .vel.y = 0, .r = 15};

	Vector2D 	mouse,
				ai_stick_pos = ai_default_stick_pos,
				stick_pos ={.x = 250, .y  = 600},
				stick_vel = {.x = 0, .y =0};

	int score_player = 0;
	int score_ai = 0;

	int isplayed = 1;
	int continuedraw = 1;

	menu:
	while(1) {

		ALLEGRO_MOUSE_STATE state;		//Get mouse position
		al_get_mouse_state(&state); 	//

		al_clear_to_color(WHITE);

		draw_menu();

		if(state.buttons&1){
			if(state.x >= screen.x/2-125 && state.y >= 250 &&
			state.x <= screen.x/2+125 && state.y <= 250+60){
				printf("gameplay\n");
				goto diff;
			}
			/*if(state.x >= screen.x/2-125, 250+100 && state.y >= 250+100 &&
			state.x <= screen.x/2+125 && state.y <= 250+160){
				printf("sett\n");
				//goto settings;
			}*/
			if(state.x >= screen.x/2-125 && state.y >= 250+200 &&
			state.x <= screen.x/2+125 && state.y <= 250+260){
				printf("end\n");
				goto end;
			}
		}
		al_flip_display();
	}

	diff:
	while(1) {

		
		ALLEGRO_MOUSE_STATE state;		//Get mouse position
		al_get_mouse_state(&state); 	//

		if(state.buttons&1&&continuedraw){
			continue;
		}
		else{
			continuedraw = 0;
		}

		al_clear_to_color(WHITE);

		draw_diff();

		if(state.buttons&1){
			if(state.x >= screen.x/2-125 && state.y >= 250 &&
			state.x <= screen.x/2+125 && state.y <= 250+60){
				stick_ai_v = 1100;
				goto gameplay;
			}
			if(state.x >= screen.x/2-125, 250+100 && state.y >= 250+100 &&
			state.x <= screen.x/2+125 && state.y <= 250+160){
				stick_ai_v = 1500;
				goto gameplay;
			}
			if(state.x >= screen.x/2-125 && state.y >= 250+200 &&
			state.x <= screen.x/2+125 && state.y <= 250+260){
				stick_ai_v = 1900;
				goto gameplay;
			}
		}
		al_flip_display();
	}

	winorlose:
	while(1) {

		ALLEGRO_MOUSE_STATE state;		//Get mouse position
		al_get_mouse_state(&state); 	//

		al_clear_to_color(WHITE);

		if(score_ai==10)
		draw_bot_win();
		if (score_player==10)
		draw_player_win();
		

		if(state.buttons&1){
			/*if(state.x >= screen.x/2-125 && state.y >= 250 &&
			state.x <= screen.x/2+125 && state.y <= 250+60){
				printf("gameplay\n");
				goto diff;
			}*/
			if(state.x >= screen.x/2-125, 250+100 && state.y >= 250+100 &&
			state.x <= screen.x/2+125 && state.y <= 250+160){
				printf("again\n");
				score_ai = 0;
				score_player = 0;
				continuedraw = 1;
				goto diff;
			}
			if(state.x >= screen.x/2-125 && state.y >= 250+200 &&
			state.x <= screen.x/2+125 && state.y <= 250+260){
				printf("end\n");
				goto end;
			}
		}
		al_flip_display();
	}


	gameplay:
	while (1) {

		al_wait_for_event(queue, &ev);

		if(ev.type==ALLEGRO_EVENT_TIMER){

			al_clear_to_color(al_map_rgb(255, 255, 255));
			///////////////////////////////// User Puck 
			ALLEGRO_MOUSE_STATE state;		//Get mouse position
			al_get_mouse_state(&state); 	//

			mouse.x = state.x;
			mouse.y = state.y;
		
			if (mouse.x==0||mouse.y<600&&isplayed){
				mouse.x = 250;
				mouse.y = 600;
			}
			else{
				isplayed = 0;
			}

			Box player_arena = arena;
			player_arena.c1.y = 400-25;

			mouse = find_position_optimized(mouse, player_arena, stick_r);

			Vector2D stick_mouse_x2d = sub(mouse, stick_pos);
			Vector2D stick_mouse_v2d = division(stick_mouse_x2d, dt);
			Vector2D stick_mouse_dir = nor(stick_mouse_x2d);
			
			Vector2D stick_max_v2d = mul(stick_mouse_dir, stick_max_v);

			Vector2D stick_actual_vel;

			if(stick_max_v<len(stick_mouse_v2d)){
				stick_actual_vel = stick_max_v2d;
			}
			else {
				stick_actual_vel = stick_mouse_v2d;
			}

			/////////////////////////////////// AI PUCK
			Vector2D target;

			if(puck.pos.y<arena.c1.y+300){//&&puck.pos.y>ai_stick_pos.y
				target = puck.pos;
			}
			else{
				target = ai_default_stick_pos;
			}

			Vector2D stick_ai_x2d = sub(target, ai_stick_pos);
			Vector2D stick_ai_v2d = division(stick_ai_x2d, dt);
			Vector2D stick_ai_dir = nor(stick_ai_x2d);
			
			Vector2D stick_ai_max_v2d = mul(stick_ai_dir, stick_ai_v);

			Vector2D stick_ai_actual_vel;

			if(stick_ai_v<len(stick_ai_v2d)){
				stick_ai_actual_vel = stick_ai_max_v2d;
			}
			else {
				stick_ai_actual_vel = stick_ai_v2d;
			}

			//Collision
			collision_with_stick(stick_pos, puck.pos, stick_r, puck.r, &stick_actual_vel, &puck.vel, stick_m, puck_m);
			collision_with_stick(ai_stick_pos, puck.pos, stick_r, puck.r, &stick_ai_actual_vel, &puck.vel, stick_m, puck_m);
			collision_with_wall(puck.pos, &puck.vel, arena, puck.r);

			//Slowing 
			puck.vel= mul(nor(puck.vel), len(puck.vel)*0.9925f);

			//Euler integration
			ai_stick_pos = add(ai_stick_pos, mul(stick_ai_actual_vel, dt));
			stick_pos = add(stick_pos, mul(stick_actual_vel, dt));
			puck.pos = add(puck.pos, mul(puck.vel, dt));

			int score = collision_with_goal(puck.pos, 190, 310, puck.r);

			if(score == 1){
				score_player++;
				printf("Score is %d %d\n", score_player, score_ai);

				puck.pos.x = 250;
				puck.pos.y = 350;

				puck.vel.x = 0;
				puck.vel.y = 0;
			}
			else if(score == 2){
				score_ai++;
				printf("Score is %d %d\n", score_player, score_ai);

				puck.pos.x = 250;
				puck.pos.y = 450;

				puck.vel.x = 0;
				puck.vel.y = 0;
			}

			if(score_ai==10||score_player==10){
				goto winorlose;
			}
			
			al_draw_rectangle(arena.c1.x, arena.c1.y, arena.c2.x, arena.c2.y, al_map_rgb(0,0,0),3);
			al_draw_line(190, arena.c1.y, 310, arena.c1.y, RED, 5);
			al_draw_line(190, arena.c2.y, 310, arena.c2.y, BLUE, 5);

			al_draw_line(arena.c1.x, (arena.c1.y+arena.c2.y)/2, (arena.c1.y+arena.c2.y)/2, arena.c2.x, ORANGE, 3);
			al_draw_circle((arena.c1.x+arena.c2.x)/2, (arena.c1.y+arena.c2.y)/2, 50, ORANGE, 3);

			al_draw_filled_circle(puck.pos.x, puck.pos.y, puck.r, al_map_rgb(0,255,0));
			al_draw_filled_circle(stick_pos.x, stick_pos.y, stick_r, al_map_rgb(0, 0, 255));
			al_draw_filled_circle(ai_stick_pos.x, ai_stick_pos.y, stick_r, al_map_rgb(255, 0, 0));

			al_draw_textf(font, BLACK, 210, 40, 0, "%d : %d", score_player, score_ai);

			al_flip_display();

		}
	}
	end:
    /* deleting the initialized macros */
    al_destroy_timer(timer);
    al_destroy_display(display);
    al_destroy_event_queue(queue);
    al_destroy_font(font);
    al_uninstall_keyboard();
    al_shutdown_primitives_addon();
    al_uninstall_system();
    printf("Game is terminated succesfully\n");
	return 0;
}
